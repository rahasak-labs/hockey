# Optimize Tea3 ML Model Accuracy

## Task

In previous matches, we implemented a Tea Price Prediction ML model using the Apache Spark DecisionTree algorithm. Details of the implementation and results can be found in the [result of Match-1](). The primary concern with the current ML model is its accuracy, which currently stands at 80% with the current dataset. The main objective of this task is to improve the accuracy of the ML model and achieve a stage of 90% or higher.

## Time

2 weeks

## Strategy

There are two main suggestions to improve the accuracy of the ML model:

1. Optimize feature selection: Currently, we are using the following features in the DecisionTree model: `subElevation, category, rp, outlotSettingType, netWeight, standard, date, week, month, year`. We need to test the accuracy of the ML algorithm by removing unwanted features. Here are some suggestions:
   - According to the dataset, the `year` feature may not contribute significantly to the accuracy. We need to identify a valid feature set that optimizes the ML algorithm's accuracy.
   - There is some missing data in the `outlotSettingType` feature. We could try removing this feature column and measure the accuracy. Additionally, we can explore omitting rows with missing values or imputing missing values with mean or mode values.

2. Try different ML algorithms: Currently, we are using the DecisionTree algorithm. We can experiment with other suggested algorithms and compare their accuracy with the current model. Here are some alternatives:
   - LogisticRegression
   - RandomForest
   - GBTRegressor

## Concerns

- The impact of feature selection may vary with a larger dataset, so we need to consider the scalability of the chosen features.
- Care should be taken when imputing missing values to avoid introducing biases.

## Nice-to-have

- Explore other methods of improving model accuracy, such as data augmentation, ensemble methods, or hyperparameter tuning.

## References

- [Spark at Rahasak Labs](https://medium.com/rahasak/tagged/spark)
- [Logistic Regression with Spark](https://medium.com/rahasak/logistic-regression-with-apache-spark-b7ec4c98cfcd)
- [Random Forest with Spark](https://medium.com/rahasak/random-forest-classifier-with-apache-spark-c63b4a23a7cc)
- [8 Ways to Improve Model Accuracy](https://www.analyticsvidhya.com/blog/2015/12/improve-machine-learning-results/)

## Result

- TODO: Update here once the task is finished.
